Vagrant box and Ansible playbook to provision minimal Graphite server with Collectd client on CentOS 6.5.

Also included is a simple script I used to import historic stock data from csv download.

With Vagrant (http://vagrantup.com) and Ansible (http://ansible.com) installed, do this to get started:
```
#!bash
git clone git@bitbucket.org:wyattwalter/pymntos-graphite-demo.git
cd pymntos-graphite-demo
vagrant up
```
 Stands up basic Graphite node with port 8080 forwarded from host machine for accessing web interface. Also forwards 2003 on the host for sending data into carbon-cache.