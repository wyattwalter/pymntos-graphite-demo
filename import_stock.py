import csv
from datetime import datetime
import graphitesend

f = open('stock_data.csv')
data = csv.reader(f)

graphite = graphitesend.init(graphite_server='localhost', prefix='stock', system_name='')

for row in data:
    company = row[0].lower()
    date    = datetime.strptime(row[1], '%d-%b-%Y').strftime('%s')
    price   = row[2]
    #print 'date: {0}, company: {1}'.format(date, company)
    graphite.send(company, price, timestamp=date)

